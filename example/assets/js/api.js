/* 
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


$(function() {
	$('form').submit(function() {
		var form = $(this);
		var params = {
			post: 1
		};
		var all = true;
		
		if (form.attr('method') === 'get') {
			return true;
		}
		
		form.find('input,select,textarea').each(function() {
			var t = $(this);
			
			if (t.attr('type') !== 'submit') {
				if ($.trim(t.val())) {
					if (t.attr('type') !== 'radio' || t.is(':checked')) {
						params[t.attr('name')] = t.val();
					}
				}
				else if (t.attr('aria-required') === 'true') {
					all = false;
				}
			}
		});
		
		if (all) {
			$.ajax(form.attr('action'), {
				method: 'POST',
				data: params,
				success: function(data) {
					if (data.status === 0 && data.log) {
						console.log(data.log);
					}
					
					if (data.action === 'click') {
						$('#' + data.objId).trigger('click');
					}
					else if (data.action === 'alert') {
						alert(data.message);
					}
					else if (data.action === 'gateway') {
						window.location = data.url;
					}
					else if (data.action === 'html') {
						$('#' + data.objId).html(data.content);
					}
				}
			});
		}
		
		return false;
	});
	
	$(function() {
		if (window.location.hash === '#three') {
			$('#three .content p').html('Please wait...');
			
			$.ajax($('#three').attr('data-action'), {
				method: 'POST',
				data: window.location.search.replace('?', ''),
				success: function(data) {
					if (data.status === 0 && data.log) {
						console.log(data.log);
					}
					
					if (data.action === 'alert') {
						alert(data.message);
					}
					else {
						$('#three .goto-next').animate({
							opacity: 1
						});
						$('#three .content p').hide().after(data.content);
					}
				}
			});
		}
	});
});