<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

session_start();

require_once realpath(__DIR__ . '/../server/autoload.php');

$result = new stdClass();
$result->status = 0;
$result->log = null;

if ($_POST) {
	try {
		if (!(isset($_POST["_nooledge"]["uid"]) 
			&& isset($_POST["_nooledge"]["res"]) 
			&& isset($_POST["_nooledge"]["sig"])
			&& is_string($_POST["_nooledge"]["uid"])
			&& is_string($_POST["_nooledge"]["res"])
			&& is_string($_POST["_nooledge"]["sig"]))) {
			throw new \Exception("Some required POST parameter is missing!");
		}
		
		$c = \NooledgeApiExample\Config::load();
		$config = new \NooledgeApiClient\Config();
		$config
			->setAppId($c->getAppId())
			->setSecretKey($c->getSecretKey())
			->setApiHost($c->getApiHost());

		$signature = hash("sha256", $config->getAppId() . $_POST["_nooledge"]["uid"] . $_POST["_nooledge"]["res"] . $config->getSecretKey());
		
		if ($signature != $_POST["_nooledge"]["sig"]) {
			throw new \Exception("Result signature is invalid!");
		}
		
		$auth = new \NooledgeApiClient\AuthToken($config);
		$auth->setConnectUid($_POST["_nooledge"]["uid"]);
		$tokenData = $auth->makeRequest();
		
		// Save $tokenData->access_token & $tokenData->refresh_token securely in your database
		// This example uses sessions instead
		\NooledgeApiExample\CredentialsSession::saveToken($tokenData->access_token, $tokenData->refresh_token);

		$result->status = 1;
		$result->action = "html";
		$result->content = "<p><strong>Excellent! You are signed in! :-)</strong><br />"
			. "Accepted scope: " . implode(", ", $tokenData->scope) . "<br />"
			. "Access token will expire {$tokenData->expiration}</p>";
	}
	catch (\Exception $e) {
		$result->log = $e->getMessage();
		$result->action = "alert";
		$result->message = "Something is wrong (more in browser console).";
	}
}

header("Content-Type: application/json");
print_r(json_encode($result));