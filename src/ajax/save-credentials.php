<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

session_start();

require_once realpath(__DIR__ . '/../server/autoload.php');

$result = new stdClass();
$result->status = 0;
$result->log = null;

if ($_POST && isset($_POST["app_id"]) && isset($_POST["secret_key"])) {
	try {
		\NooledgeApiExample\CredentialsSession::store($_POST["app_id"], $_POST["secret_key"]);

		$result->status = 1;
		$result->action = "click";
		$result->objId = "next_1";
	}
	catch (\Exception $e) {
		$result->log = $e->getMessage();
	}
}

header("Content-Type: application/json");
print_r(json_encode($result));