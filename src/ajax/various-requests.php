<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

session_start();

require_once realpath(__DIR__ . '/../server/autoload.php');

$result = new stdClass();
$result->status = 0;
$result->log = null;

if ($_POST) {
	try {
		if (!(isset($_POST["request_type"]) && in_array($_POST["request_type"], ["refresh_token", "myself", "my_followers", "my_channels", "user", "channel", "object"]))) {
			throw new \Exception("Some required POST parameter is missing or is invalid!");
		}
		
		$resultContent = "";
		$rt = $_POST["request_type"];
		
		$c = \NooledgeApiExample\Config::load();
		$config = new \NooledgeApiClient\Config();
		$config
			->setAppId($c->getAppId())
			->setSecretKey($c->getSecretKey())
			->setApiHost($c->getApiHost());
		
		$accessToken = \NooledgeApiExample\CredentialsSession::getAccessToken();
		$refreshToken = \NooledgeApiExample\CredentialsSession::getRefreshToken();
		
		if ($rt == "refresh_token") {
			if (!$refreshToken) {
				throw new \Exception("There is no refresh_token!");
			}
			
			$userId = \NooledgeApiExample\CredentialsSession::getUser();
			
			if (!$userId) {
				throw new \Exception("There is no user ID in the session! Execute /me first.");
			}
			
			$auth = new \NooledgeApiClient\AuthTokenRefresh($config);
			$auth->setUserId($userId);
			$auth->setRefreshToken($refreshToken);
			$tokenData = $auth->makeRequest();
			
			\NooledgeApiExample\CredentialsSession::saveToken($tokenData->access_token, $tokenData->refresh_token);
			
			$resultContent = "A new access token was stored.";
		}
		elseif ($rt == "myself") {
			$auth = new \NooledgeApiClient\Me($config);
			$auth->setAccessToken($accessToken);
			$resultData = $auth->makeRequest();
			$resultContent = htmlspecialchars(json_encode($resultData));
			
			\NooledgeApiExample\CredentialsSession::saveUser($resultData->id);
		}
		elseif ($rt == "my_followers") {
			$auth = new \NooledgeApiClient\MyFollowers($config);
			$auth->setAccessToken($accessToken);
			$resultData = $auth->makeRequest();
			$resultContent = htmlspecialchars(json_encode($resultData));
		}
		elseif ($rt == "my_channels") {
			$auth = new \NooledgeApiClient\MyChannels($config);
			$auth->setAccessToken($accessToken);
			$resultData = $auth->makeRequest();
			$resultContent = htmlspecialchars(json_encode($resultData));
		}
		elseif ($rt == "user") {
			$userId = isset($_POST["user_id"]) ? $_POST["user_id"] : 0;
			$method = isset($_POST["user_method"]) ? $_POST["user_method"] : "";
			
			if (!$userId) {
				throw new \Exception("Please type a valid USER_ID!");
			}
			
			if (!in_array($method, ["", "followers", "channels"])) {
				throw new \Exception("Invalid method!");
			}
			
			$auth = new \NooledgeApiClient\User($config);
			$auth->setUserId($userId);
			$auth->setMethod($method);
			$resultData = $auth->makeRequest();
			$resultContent = htmlspecialchars(json_encode($resultData));
		}
		elseif ($rt == "channel") {
			$channelId = isset($_POST["channel_id"]) ? $_POST["channel_id"] : 0;
			$method = isset($_POST["channel_method"]) ? $_POST["channel_method"] : "";
			
			if (!$channelId) {
				throw new \Exception("Please type a valid CHANNEL_ID!");
			}
			
			if (!in_array($method, ["", "followers", "objects"])) {
				throw new \Exception("Invalid method!");
			}
			
			$auth = new \NooledgeApiClient\Channel($config);
			$auth->setChannelId($channelId);
			$auth->setMethod($method);
			$resultData = $auth->makeRequest();
			$resultContent = htmlspecialchars(json_encode($resultData));
		}
		elseif ($rt == "object") {
			$objectId = isset($_POST["object_id"]) ? $_POST["object_id"] : 0;
			$method = isset($_POST["object_method"]) ? $_POST["object_method"] : "";
			
			if (!$objectId) {
				throw new \Exception("Please type a valid OBJECT_ID!");
			}
			
			if (!in_array($method, [""])) {
				throw new \Exception("Invalid method!");
			}
			
			$auth = new \NooledgeApiClient\Object($config);
			$auth->setObjectId($objectId);
			$auth->setMethod($method);
			$resultData = $auth->makeRequest();
			$resultContent = htmlspecialchars(json_encode($resultData));
		}
		
		$result->status = 1;
		$result->action = "html";
		$result->objId = "four_results";
		$result->content = "<pre><code>{$resultContent}</code></pre>";
	}
	catch (\Exception $e) {
		$result->log = $e->getMessage();
		$result->action = "alert";
		$result->message = "Something is wrong (more in browser console).";
	}
}

header("Content-Type: application/json");
print_r(json_encode($result));