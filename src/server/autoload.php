<?php

/* 
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

class NooledgeApiClientAutoloader {
	public static function loadClass($class) {
		if (!self::_clientClass($class)) {
			self::_exampleClass($class);
		}
	}
	
	public static function register() {
		spl_autoload_register(array("NooledgeApiClientAutoloader", "loadClass"), true, true);
	}
	
	private static function _clientClass($class) {
		$matches = array();
		
		if (preg_match("/^NooledgeApiClient\\\(.+)$/", $class, $matches)) {
			require_once __DIR__ . "/nooledge-api-php-client/" . str_replace("\\", "/", $matches[1]) . ".php";
			return true;
		}
		
		return false;
	}
	
	private static function _exampleClass($class) {
		$matches = array();
		
		if (preg_match("/^NooledgeApiExample\\\(.+)$/", $class, $matches)) {
			require_once __DIR__ . "/nooledge-api-example/" . str_replace("\\", "/", $matches[1]) . ".php";
			return true;
		}
		
		return false;
	}
}

NooledgeApiClientAutoloader::register();

require_once __DIR__ . "/vendor/autoload.php";