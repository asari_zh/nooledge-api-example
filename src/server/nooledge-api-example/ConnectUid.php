<?php

/*
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace NooledgeApiExample;

class ConnectUid {
	const DTB_DIR = "/../../../dtb";
	const DTB_FILE = "/connect-uid.json";
	const UID_PREFIX = "api-example-";
	const SESSION = "nooapi_connect_uid";
	
	public static function getNewUid() {
		$newNumber = self::readDtb() + 1;
		self::writeDtb($newNumber);
		
		$_SESSION[self::SESSION] = self::UID_PREFIX + $newNumber;
		
		return self::UID_PREFIX . md5($_SERVER["REMOTE_ADDR"]) . "/" . $_SESSION[self::SESSION];
	}
	
	private static function readDtb() {
		$file = self::getFile();
		$lastNumber = 0;
		
		if (is_file($file)) {
			$content = file_get_contents($file);
			
			if ($content) {
				$std = json_decode($content, true);
				
				if (isset($std["lastNumber"])) {
					$lastNumber = $std["lastNumber"];
				}
			}
		}
		
		return $lastNumber;
	}
	
	private static function writeDtb($newNumber) {
		$data = array(
			"lastNumber" => $newNumber
		);
		
		return file_put_contents(self::getFile(), json_encode($data));
	}
	
	private static function getFile() {
		$dir = realpath(__DIR__ . self::DTB_DIR);
		
		if (!is_writable($dir)) {
			throw new \Exception("Directory '{$dir}' is not writable!");
		}
		
		return $dir . self::DTB_FILE;
	}
}
