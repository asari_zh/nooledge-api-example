<?php

/*
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace NooledgeApiExample;

class CredentialsSession {
	const APP_ID = "nooapi_app_id";
	const SECRET_KEY = "nooapi_secret_key";
	const ACCESS_TOKEN = "nooapi_at";
	const REFRESH_TOKEN = "nooapi_rt";
	const USER_ID = "nooapi_user";
	
	public static function store($appId, $secretKey) {
		$_SESSION[self::APP_ID] = $appId;
		$_SESSION[self::SECRET_KEY] = $secretKey;
	}
	
	public static function getAppId() {
		return (isset($_SESSION[self::APP_ID]) ? $_SESSION[self::APP_ID] : null);
	}
	
	public static function getSecretKey() {
		return (isset($_SESSION[self::SECRET_KEY]) ? $_SESSION[self::SECRET_KEY] : null);
	}
	
	public static function saveToken($accessToken, $refreshToken) {
		$_SESSION[self::ACCESS_TOKEN] = $accessToken;
		$_SESSION[self::REFRESH_TOKEN] = $refreshToken;
	}
	
	public static function getAccessToken() {
		return (isset($_SESSION[self::ACCESS_TOKEN]) ? $_SESSION[self::ACCESS_TOKEN] : null);
	}
	
	public static function getRefreshToken() {
		return (isset($_SESSION[self::REFRESH_TOKEN]) ? $_SESSION[self::REFRESH_TOKEN] : null);
	}
	
	public static function saveUser($userId) {
		$_SESSION[self::USER_ID] = $userId;
	}
	
	public static function getUser() {
		return (isset($_SESSION[self::USER_ID]) ? $_SESSION[self::USER_ID] : null);
	}
}
