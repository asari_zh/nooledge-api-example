<?php

/*
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace NooledgeApiClient;

class AuthConnect extends Request {
	const GRANT_ACCESS_TOKEN = 1;
	const GRANT_REFRESH_TOKEN = 2;
	const GRANT_EMAIL = 4;
	const GRANT_APP_BUTTON = 8;
	
	const RETURN_PARAM = "_nooledge";
	
	const PATH = "/auth/connect";
	
	private $connectUid;
	private $grantType;
	private $returnParam;
	private $returnUrl;
	
	public function setConnectUid($uid) {
		$this->connectUid = $uid;
	}
	
	public function setGrantType($grant = self::GRANT_ACCESS_TOKEN) {
		$this->grantType = $grant;
	}
	
	public function setReturnParam($param) {
		$this->returnParam = $param;
	}
	
	public function setReturnUrl($url) {
		$this->returnUrl = $url;
	}
	
	public function makeRequest() {
		$this->method = self::POST;
		$this->path = self::PATH;
		
		$this->addPostField("app_id", $this->config->getAppId());
		$this->addPostField("connect_uid", $this->connectUid);
		$this->addPostField("grant_type", $this->prepareGrantType());
		$this->addPostField("return_url", $this->returnUrl);
		
		if ($this->returnParam) {
			$this->addPostField("return_param", $this->returnParam);
		}
		
		$this->addPostField("signature", $this->generateSignature($this->getPostFieldsAsString()));
		
		$this->execute();
		$this->CheckResponseSignature();
		
		// You can also save our ID of your request (for issues) = $this->responseHeaders["X-Nooledge-API-ID"]
		
		return $this->responseData->gateway_url;
	}
	
	private function prepareGrantType() {
		$gtMap = [
			self::GRANT_ACCESS_TOKEN => "access_token",
			self::GRANT_REFRESH_TOKEN => "refresh_token",
			self::GRANT_EMAIL => "email",
			self::GRANT_APP_BUTTON => "app_button"
		];
		
		$grantType = [];
		$grantType[] = $gtMap[self::GRANT_ACCESS_TOKEN];
		
		foreach ($gtMap as $gt => $type) {
			if ($gt != self::GRANT_ACCESS_TOKEN) {
				if ($this->grantType & $gt) {
					$grantType[] = $type;
				}
			}
		}
		
		return implode(",", $grantType);
	}
	
	private function CheckResponseSignature() {
		if (isset($this->responseHeaders["X-Nooledge-API-Signature"])) {
			if ($this->generateSignature($this->responseData->gateway_url . $this->responseData->expiration) != $this->responseHeaders["X-Nooledge-API-Signature"]) {
				throw new \Exception("API Error: Invalid response signature");
			}
		}
		else {
			throw new \Exception("API Error: No response signature");
		}
	}
}
