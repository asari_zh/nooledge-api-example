<?php

/*
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace NooledgeApiClient;

class AuthToken extends Request {
	const PATH = "/auth/token";
	
	private $connectUid;
	
	public function setConnectUid($uid) {
		$this->connectUid = $uid;
	}
	
	public function makeRequest() {
		$this->method = self::POST;
		$this->path = self::PATH;
		
		$this->addPostField("app_id", $this->config->getAppId());
		$this->addPostField("connect_uid", $this->connectUid);
		$this->addPostField("signature", $this->generateSignature($this->getPostFieldsAsString()));
		
		$this->execute();
		$this->CheckResponseSignature();
		
		return $this->responseData;
	}
	
	private function CheckResponseSignature() {
		if (isset($this->responseHeaders["X-Nooledge-API-Signature"])) {
			if ($this->generateSignature($this->responseData->access_token . $this->responseData->expiration . $this->responseData->refresh_token) != $this->responseHeaders["X-Nooledge-API-Signature"]) {
				throw new \Exception("API Error: Invalid response signature");
			}
		}
		else {
			throw new \Exception("API Error: No response signature");
		}
	}
}
