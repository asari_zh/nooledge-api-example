<?php

/*
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace NooledgeApiClient;

class Channel extends Request {
	const PATH = "/channel/%d";
	const PATH2 = "/channel/%d/%s";
	
	private $channelId;
	private $m;
	private $referenceCode;
	private $types;
	private $page;
	
	public function setChannelId($channelId) {
		$this->channelId = $channelId;
	}
	
	public function setMethod($method) {
		$this->m = $method;
	}
	
	public function setReferenceCode($ref) {
		$this->referenceCode = $ref;
	}
	
	public function setTypes($types) {
		$this->types = $types;
	}
	
	public function setPage($page) {
		$this->page = $page;
	}
	
	public function makeRequest() {
		$this->method = self::GET;
		$this->path = ($this->m ? sprintf(self::PATH2, $this->channelId, $this->m) : sprintf(self::PATH, $this->channelId));
		
		if ($this->referenceCode) {
			$this->addQueryField("ref", $this->referenceCode);
		}
		
		if (is_array($this->types) && $this->m == "objects") {
			$this->addQueryField("type", implode(",", $this->types));
		}
		
		if ($this->page > 0 && $this->m == "objects") {
			$this->addQueryField("page", $this->page);
		}
		
		$this->addHeaderField("App-ID", $this->config->getAppId());
		$this->addHeaderField("Signature", $this->generateSignature($this->config->getAppId() . $this->channelId));
		
		$this->execute();
		
		return $this->responseData;
	}
}
