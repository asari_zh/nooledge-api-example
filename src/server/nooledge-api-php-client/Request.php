<?php

/*
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace NooledgeApiClient;

abstract class Request {
	const GET = "GET";
	const POST = "POST";
	const PUT = "PUT";
	const DELETE = "DELETE";
	
	const USER_AGENT = "Nooledge-API-PHP-Client/1.0";
	const TIMEOUT = 15;
	
	protected $path;
	protected $method;
	protected $config;
	protected $responseHeaders;
	protected $responseData;
	private $postFields;
	private $query;
	private $headers;
	
	final public function __construct(Config &$config) {
		$this->config =& $config;
	}
	
	final protected function execute() {
		$c = curl_init();
		
		if ($c) {
			curl_setopt($c, CURLOPT_URL, $this->config->getApiHost() . $this->path . $this->prepareQuery());
			curl_setopt($c, CURLOPT_CUSTOMREQUEST, $this->method);
			curl_setopt($c, CURLOPT_USERAGENT, self::USER_AGENT);
			curl_setopt($c, CURLOPT_VERBOSE, 1);
			
			if ($this->checkHttps()) {
				curl_setopt($c, CURLOPT_SSL_VERIFYPEER, 1);
				curl_setopt($c, CURLOPT_SSL_VERIFYHOST, 2);
			}
			
			curl_setopt($c, CURLOPT_RETURNTRANSFER, 1);
			curl_setopt($c, CURLOPT_HEADER, 1);
			curl_setopt($c, CURLOPT_TIMEOUT, self::TIMEOUT);
			
			if ($this->postFields) {
				curl_setopt($c, CURLOPT_POSTFIELDS, $this->postFields);
			}
			
			if ($this->headers) {
				$headers = [];
				
				foreach ($this->headers as $hk => $hv) {
					$headers[] = $hk . ": " . $hv;
				}
				
				curl_setopt($c, CURLOPT_HTTPHEADER, $headers);
			}
			
			$response = curl_exec($c);
			$result = curl_getinfo($c);
			
			if (curl_errno($c)) {
				curl_close($c);
				throw new \Exception("API System Error: " . curl_error($c), curl_errno($c));
			}
			
			if ($response) {
				$headerSize = $result["header_size"];
				$header = substr($response, 0, $headerSize);
				$body = substr($response, $headerSize);
				
				$this->parseHeaders($header);
				$this->parseResponse($body, $result["http_code"]);
			}
			else {
				curl_close($c);
				throw new \Exception("API System Error: Empty response");
			}
			
			curl_close($c);
		}
	}
	
	final protected function addPostField($key, $value) {
		if (!$this->postFields) {
			$this->postFields = [];
		}
		
		$this->postFields[$key] = $value;
	}
	
	final protected function getPostFieldsAsString() {
		return implode("", $this->postFields);
	}
	
	final protected function addQueryField($key, $value) {
		if (!$this->query) {
			$this->query = [];
		}
		
		$this->query[$key] = $value;
	}
	
	final protected function prepareQuery() {
		if ($this->query) {
			return "?" . http_build_query($this->query);
		}
		else {
			return "";
		}
	}
	
	final protected function addHeaderField($key, $value) {
		if (!$this->headers) {
			$this->headers = [];
		}
		
		$this->headers["X-Nooledge-" . $key] = $value;
	}
	
	final protected function getHeaderFieldsAsString() {
		return implode("", $this->headers);
	}
	
	final protected function generateSignature($string) {
		return hash("sha256", $string . $this->config->getSecretKey());
	}
	
	private function checkHttps() {
		if (strstr($this->config->getApiHost(), "https://") !== false) {
			return true;
		}
		else {
			return false;
		}
	}
	
	private function parseHeaders($headers) {
		$array = explode("\r\n", $headers);
		$xNooledge = array();
		
		if (count($array) > 0) {
			foreach ($array as $header) {
				if ($header) {
					if (strstr($header, "X-Nooledge-") !== false) {
						list($key, $value) = explode(":", $header);
						$xNooledge[trim($key)] = trim($value);
					}
				}
			}
		}
		
		$this->responseHeaders = $xNooledge;
	}
	
	private function parseResponse($body, $httpCode) {
		$reponseData = json_decode($body);
		
		if (!$reponseData) {
			throw new \Exception("API System Error: Invalid response");
		}
		
		if ($httpCode == 200) {
			if ($reponseData->result->code == $httpCode) {
				$this->responseData = $reponseData;
			}
			else {
				throw new \Exception("API System Error: Invalid result code");
			}
		}
		else {
			throw new \Exception("API Error: {$reponseData->private_message}" . ($reponseData->code ? " [{$reponseData->code}]" : ""));
		}
	}
}
