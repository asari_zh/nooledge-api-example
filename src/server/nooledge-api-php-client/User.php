<?php

/*
 * The MIT License
 *
 * Copyright 2016 Asari Technologies Ltd.
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */

namespace NooledgeApiClient;

class User extends Request {
	const PATH = "/user/%d";
	const PATH2 = "/user/%d/%s";
	
	private $userId;
	private $m;
	private $referenceCode;
	
	public function setUserId($userId) {
		$this->userId = $userId;
	}
	
	public function setMethod($method) {
		$this->m = $method;
	}
	
	public function setReferenceCode($ref) {
		$this->referenceCode = $ref;
	}
	
	public function makeRequest() {
		$this->method = self::GET;
		$this->path = ($this->m ? sprintf(self::PATH2, $this->userId, $this->m) : sprintf(self::PATH, $this->userId));
		
		if ($this->referenceCode) {
			$this->addQueryField("ref", $this->referenceCode);
		}
		
		$this->addHeaderField("App-ID", $this->config->getAppId());
		$this->addHeaderField("Signature", $this->generateSignature($this->config->getAppId() . $this->userId));
		
		$this->execute();
		
		return $this->responseData;
	}
}
